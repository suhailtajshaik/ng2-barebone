import {Component} from 'angular2/core';

@Component({
    selector: 'my-app',
    template: `<h1>Angular 2</h1> <br/><h2>{{msg}}</h2><br/><p>Source: <a href= "https://bitbucket.org/suhailtaj/ng2-barebone/src/25a99eb61b38?at=master"> Suhail Taj Repository</a></p>`
})
export class AppComponent { 
msg = "Congratulations you have done it!"
}
